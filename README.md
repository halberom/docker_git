[![Build Status](https://travis-ci.org/halberom/docker_git.svg?branch=master)](https://travis-ci.org/halberom/docker_git)

Example run

    $ docker run --rm -it --name git halberom/git version

Though you probably want to add a volume mount to it.
